# Discord Transcription Bot
A Discord bot that uses slash commands to transcribe voice chats using OpenAI's Whisper!

## Usage
1. Clone the repository
2. Copy the `config.example.js` file to `config.js` and [fill in the required values](#configuration)
3. Invite the bot to your server with the required [permissions](#bot-permissions)
4. Install the necessary [dependencies](#dependencies)
5. Start the bot with `npm run pm2-start`
6. Create a voice channel and a text channel for transcription
7. Execute the `/record` command from the text channel

### Slash Commands
* `/transcribe` `<#voice-channel>` - Execute the command in a text channel; the bot will enter the designated voice channel and begin transcribing its content into the text channel
* `/stop` - Stop the recording
* `/reboot` - Reboot the bot (only works if the bot is running with PM2)

### Dependencies
* [ffmpeg](https://ffmpeg.org/)
* [OpenAI API](https://platform.openai.com/docs/api-reference/audio)
* [PM2](https://pm2.keymetrics.io/) (optional) - For running the bot in the background

### Configuration
Create a `config.js` file in the root directory by copying the contents of `config.example.js` and filling in the necessary values:
* `discordAPIKey`: Discord Application's token ([Discord Developer Portal](https://discord.com/developers/applications) > "Bot" > "Token")
* `botUserID`: Discord Application's ID ([Discord Developer Portal](https://discord.com/developers/applications) > "General Information" > Application ID)
* `openai_api_key`: [https://platform.openai.com/account/api-keys](https://platform.openai.com/account/api-keys)

## Bot Permissions

The bot requires the following permissions:
* General Permissions
  * Read Messages/View Channels
* Text Permissions
  * Send Messages
  * Send TTS Messages
  * Read Message History
* Voice Permissions
  * Connect
  * Speak
  * Use Voice Activity

### Privleged Intents
* PRESENCE INTENT is required for the bot status to be set


