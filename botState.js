
const { ActivityType } = require('discord.js');

// Set Activity to "Listening to nobody" and status to "Idle"
function setBotStateToIdle(client){
	client.user.setPresence({
		activities: [{ name: `nobody`, type: ActivityType.Listening }],
		status: 'idle',
	});
}

// Set Activity to "Listening to #<channel>" and status to "Online"
function setBotStateToListening(client, channelName) {
	client.user.setPresence({
		activities: [{ name: `#${channelName}`, type: ActivityType.Listening }],
		status: 'online'
	});
}

// Set Activity to "PM2 Reboot" and status to "Do Not Disturb"
function setBotStateToRebooting(client) {;
	client.user.setPresence({
		activities: [{ name: `PM2 Reboot`, type: ActivityType.Playing }],
		status: 'dnd'
	});
}

module.exports = {
	setBotStateToIdle,
	setBotStateToListening,
    setBotStateToRebooting
};