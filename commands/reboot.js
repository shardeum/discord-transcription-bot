const log = require('npmlog');
const { SlashCommandBuilder } = require('discord.js');
const botState = require('../botState.js');
const config = require('../config.js');
const pm2 = require('pm2');


module.exports = {
	data: new SlashCommandBuilder().setName('reboot').setDescription('reboot the bot service if running in PM2.'),
	async execute(interaction) {

		// Check if the bot is running under PM2
		if (process.env.pm_id) {
			
			

			await interaction.reply({ content: 'Rebooting', ephemeral: true });
			log.info('reboot', 'Rebooting bot...');
			// Set the bot state to dnd and show Rebooting in the activity
			botState.setBotStateToRebooting(interaction.client);
			
			// Connect to the PM2 daemon
			pm2.connect((err) => {
				if (err) {
					console.error(err);
					process.exit(2);
				}
				
				// Restart the bot using its PM2 process name or ID
				pm2.restart(process.env.pm_id, (err, apps) => {
					rebootSuccess = true;
					if (err) {
						console.error(err);
					}
					
					// Disconnect from the PM2 daemon
					pm2.disconnect();
				});
			});
			
		} else {
			await interaction.reply({ content: 'The bot was not started using PM2. Cannot restart.', ephemeral: true });
		}
	},
};