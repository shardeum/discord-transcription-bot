const log = require('npmlog');
const { SlashCommandBuilder, ChannelType } = require('discord.js');
const { joinVoiceChannel, EndBehaviorType } = require('@discordjs/voice');
const { opus } = require('prism-media');
const util = require('../util.js');
const fs = require('fs');
const { transcribe, heyChatGPT } = require('../openai.js');
const botState = require('../botState.js');

// check if the text contains "hey chatgpt"
function containsHeyChatGPT(text) {
	const punctuationAndWhitespaceRegex = /[!.,?;:'"`\-_~@#$%^&*()+=|/\\[\]{}<>]|\s/g;
	const cleanedText = text.replace(punctuationAndWhitespaceRegex, '').toLowerCase();
	if (cleanedText.includes('heychatgpt')) {
		return true;
	}
	return false;
}

// Subscribes to new users speaking in the voice channel
async function handleSubscription(interaction, connection, userId) {
	const fetchedGuildUser = await interaction.guild.members.fetch(userId, { force: true });
	const user = fetchedGuildUser.user;
	const userNameOfSpeaker = fetchedGuildUser.user.username;

	log.info(userNameOfSpeaker + ' started speaking');

	if (!connection.receiver.subscriptions.get(userId)) {
		log.info('Adding new subscription for ' + userNameOfSpeaker + ' started speaking');

		const userRecordingWriteStream = await util.createNewRecordingForUser(userId);

		const audioStream = connection.receiver.subscribe(userId, {
			end: {
				behavior: EndBehaviorType.AfterSilence,
				duration: 1000
			}
		});

		const decoder = new opus.Decoder({ frameSize: 960, channels: 2, rate: 48000 });
		const rawAudio = await audioStream.pipe(decoder);

		rawAudio.pipe(userRecordingWriteStream);

		audioStream.on('end', async () => {
			await handleAudioStreamEnd(interaction, user, userRecordingWriteStream);
		});
	} else {
		log.info('Subscription already exists for ' + userNameOfSpeaker);
	}
}

// On Speaking End process convert to mp3 and transcribe
async function handleAudioStreamEnd(interaction, user, userRecordingWriteStream) {
	const currentDate = new Date();

	log.info(user.username + ' stopped speaking');
	await userRecordingWriteStream.destroy();
	const recordingFilePath = userRecordingWriteStream.path;
	const outputMP3FilePath = recordingFilePath.replace('.pcm', '.mp3');

	try {
		await util.convertFile(recordingFilePath, outputMP3FilePath, async function (err) {
			if (err) {
				log.error(err);
				interaction.channel.send({ content: 'An error occurred processing the recording!' });
				return;
			}
			log.info('Conversion Complete');

			const formattedTime = currentDate.toLocaleTimeString();
			const text = await transcribe(outputMP3FilePath, "en");

			if (text !== "") {
				interaction.channel.send(`[${formattedTime}] ${user.username}: ${text}`);

				if (containsHeyChatGPT(text)) {
					const gptReply = await heyChatGPT(text);
					interaction.channel.send(`[${new Date().toLocaleTimeString()}] ChatGPT: ${user} ${gptReply}`);
				}
			}

			await deleteRecordingFiles(recordingFilePath, outputMP3FilePath);
		});
	} catch (e) {
		log.error(e);
	}
}

// clean up the recording files
async function deleteRecordingFiles(...files) {
	for (const file of files) {
		fs.unlink(file, function (err) {
			if (err) {
				console.log("Error while deleting the file" + err);
			}
			console.log("Deleted the file" + file);
		});
	}
}

module.exports = {
	data: new SlashCommandBuilder()
		.setName('transcribe')
		.setDescription('Start a transcribing!')
		.addChannelOption(option =>
			option.setName('channel')
				.setDescription('The channel you want to transcribe.')
				.setRequired(true)
				.addChannelTypes(ChannelType.GuildVoice)
		),
	async execute(interaction) {
		// Join the passed in voice channel
		const channel = interaction.options.getChannel('channel');
		const connection = await joinVoiceChannel({
			channelId: channel.id,
			guildId: interaction.guild.id,
			adapterCreator: interaction.guild.voiceAdapterCreator,
			selfDeaf: false
		});

		await interaction.reply({ content: 'Now transcribing in #' + channel.name + '!', ephemeral: true });
		log.info(`Started Transcribing in #${channel.name}`);

		// set status to online and activity to Listening to #<channel>
		botState.setBotStateToListening(interaction.client, channel.name);
		
		// Subscribe to new users speaking in the voice channel
		connection.receiver.speaking.on('start', async (userId) => {
			await handleSubscription(interaction, connection, userId);
		});
		
		return connection;
	},
};
