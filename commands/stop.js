const log = require('npmlog');
const { SlashCommandBuilder } = require('discord.js');
const { getVoiceConnection } = require('@discordjs/voice');
const botState = require('../botState.js');

module.exports = {
	data: new SlashCommandBuilder().setName('stop').setDescription('stop recording!'),
	async execute(interaction) {
		
		// clear bot activity and set status to idle
		botState.setBotStateToIdle(interaction.client);

		const currentVoiceChannel = await getVoiceConnection(interaction.guild.id);

		if (currentVoiceChannel) {

			// Leave the current voice channel
			await currentVoiceChannel.destroy();
			log.info('Stopped Recording');

			// Prevent double `reply` calls if auto-stopping recording
			if (!interaction.replied) {
				await interaction.reply({ content: 'Stopped recording!', ephemeral: true });
			}

		} else {
			await interaction.reply({ content: 'I am not recording!', ephemeral: true });
		}
	},
};